//
//  CoffeeMachine.swift
//  GR57DZ4
//
//  Created by student08 on 21.10.17.
//  Copyright © 2017 student04. All rights reserved.
//

import UIKit

class CoffeeMachine: UITableViewController {

   
    
    
    
    var waterResidue = 0
    var coffeeResidue = 0
    var freeSpaceInTray = 0
    let trayVolume = 1000
    var oldCoffe = false
    var stattCoffeMachine = false
    
    init(typeDrink: Int) {
        
        if typeDrink == 1{
            makingCoffee()
        } else if typeDrink == 2{
           makingBoilingWater()
        }
        else{
            print("Unknown drink")
        }
        
    }
    
    //приготовить кипяток
    private func makingBoilingWater(){
        if waterResidue == 0{
            errorPrint(1)
        }
        else if coffeeResidue != 0{
            errorPrint(5)
        }
        else if !stattCoffeMachine {
            errorPrint(4)
        }
        else{
            turnOff()
            print("Boiling water is ready")
        }
    }
    
    //проверка всели сделоно правильно для получения кофя
    private func makingCoffee(){
        if waterResidue == 0{
            errorPrint(1)
        }
        else if !oldCoffe {
            errorPrint(2)
        }
        else if coffeeResidue == 0{
            errorPrint(3)
        }
        else if !stattCoffeMachine {
            errorPrint(4)
        }
        else{
            turnOff()
            print("Coffee is ready")
        }
    }
    
    private func errorPrint(numberCase: Int){
        switch numberCase {
        case 1:
            print("No water")
        case 2:
            print("Old coffee! Bue")
        case 3:
            print("No coffee")
        case 4:
            print("Not included coffee machine!")
        case 5:
            print("Have coffee")
        case 6:
            print("Not water")
        case 7:
            print("Not water")
        case 8:
            print("Not water")
        case 9:
            print("Not water")
        case 10:
            print("Not water")
        default:
            print("Error")
        }
    }
    
    
    //почистить лоток
    func cleanTray(){
        coffeeResidue = 0
    }
    
    
    //Добавить кофе если не почистили до этого лоток вывести Упс
    func addCoffee(coffeeAdd: Int){
        if coffeeResidue == 0 {
            coffeeResidue = coffeeResidue + coffeeAdd
            print("Add coffe")
        }
        else{
            print("Ups")
        }
    }
    
    
    //долить воды
    func pourWater(waterCount: Int){
        
        if trayVolume => (waterResidue + waterCount) {
            print("bul bul")
            waterResidue = waterResidue + waterCount
        }
        else{
            print("plueh")
        }
    }
    
    //включить кофемашину
    func turnOn(){
        self.stattCoffeMachine = true
    }
    
    //выключить кофемашину
    func turnOff(){
        self.stattCoffeMachine = false
        
    }
}
